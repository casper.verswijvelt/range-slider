DIR_SRC = src
DIR_BUILD = build
DIR_DIST = dist
DIR_PUBLIC = public

SRC_CSS := $(DIR_SRC)/range_slider.scss
DIST_CSS := $(DIR_DIST)/range_slider.css
PUBLIC_CSS := $(DIR_PUBLIC)/range_slider.css

SRC_JS := $(DIR_SRC)/range_slider.js

DIST_MIN_JS := $(DIR_DIST)/range_slider.min.js
PUBLIC_MIN_JS := $(DIR_PUBLIC)/range_slider.min.js

NODE_BIN := node_modules/.bin

NODE_SASS := $(NODE_BIN)/node-sass
CLOSURE_COMPILER := $(NODE_BIN)/google-closure-compiler
STANDARD := $(NODE_BIN)/standard

.PHONY: all css js lint clean

all: css js

css: $(PUBLIC_CSS)

js: $(PUBLIC_MIN_JS)

$(DIST_CSS): $(SRC_CSS) | $(DIR_DIST)
	@$(NODE_SASS) --output-style compressed -o $| $<

$(PUBLIC_CSS): $(DIST_CSS)
	@cp $< $@

$(DIST_MIN_JS): $(SRC_JS) package.json package-lock.json | $(DIR_DIST)
	@$(CLOSURE_COMPILER) \
	--compilation_level ADVANCED \
	--env BROWSER \
	--language_out ECMASCRIPT5_STRICT \
	--formatting SINGLE_QUOTES \
	--externs $(DIR_BUILD)/umd.extern.js \
	--js_output_file $@ $<

$(PUBLIC_MIN_JS): $(DIST_MIN_JS)
	@cp $< $@

$(DIR_DIST):
	@mkdir -p $@

lint:
	@$(STANDARD) $(SRC_JS)

clean:
	@rm -rf $(DIR_DIST) $(PUBLIC_CSS) $(PUBLIC_MIN_JS)
