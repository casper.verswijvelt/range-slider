export = RangeSlider

declare class RangeSlider {
    constructor(options?: RangeSlider.IRangeSliderOptions)
    disabled: boolean
    config(options?: RangeSlider.IRangeSliderOptions): void
    setValue(value: number): void
    getValue(): number
    setValueHook(hook: ((currentValue: number, newValue: number) => number) | null): void
    onChange(callback: (value?: number) => void): void
    onChanged(callback: (value?: number) => void): void
    offChange(callback: (value?: number) => void): void
    offChanged(callback: (value?: number) => void): void
    removeAllChangelisteners(): void
    removeAllChangedListeners(): void
    destroy(): void
}

declare namespace RangeSlider {
    export interface IRangeSliderOptions {
        element?: HTMLElement | string
        orientation?: 'horizontal' | 'vertical'
        reverse?: boolean
        min?: number
        max?: number
        step?: number
        start?: number
        value?: number
        progress?: boolean
        disabled?: boolean
        setValueHook?: ((currentValue: number, newValue: number) => number) | null
        onChange?: (value?: number) => void
        onChanged?: (value?: number) => void
    }
}
