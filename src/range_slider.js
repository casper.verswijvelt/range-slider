/* eslint-disable dot-notation */
(function (root, factory) {
  // eslint-disable-next-line no-undef
  if (typeof define === 'function' && define.amd) {
    // AMD
    // eslint-disable-next-line no-undef
    define([], factory)

    // eslint-disable-next-line no-undef
  } else if (typeof module === 'object' && module.exports) {
    // Node
    // eslint-disable-next-line no-undef
    module.exports = factory()
  } else {
    if (!root['gidw']) root['gidw'] = {}
    // Browser globals
    root['gidw']['RangeSlider'] = factory()
  }
// eslint-disable-next-line no-undef
}(typeof self !== 'undefined' ? self : this, function () {
  'use strict'

  var _activeListenerOptions = {
    capture: false,
    passive: false
  }

  var _passiveListenerOptions = {
    capture: false,
    passive: true
  }

  /**
   * @param {TouchEvent} event
   * @param {number} id
   * @returns {?Touch}
   */
  function getTouch (event, id) {
    var touches, length, i
    touches = event.changedTouches
    length = touches.length
    for (i = 0; i < length; i++) {
      if (touches[i].identifier === id) return touches[i]
    }
    return null
  }

  /**
   * @constructor
   * @param {Object} [options]
   */
  function RangeSlider (options) {
    this._thumbTouchStartHandler = this._onThumbTouchStart.bind(this)
    this._thumbMouseDownHandler = this._onThumbMouseDown.bind(this)

    this._containerTouchStartHandler = this._onContainerTouchStart.bind(this)
    this._containerMouseDownHandler = this._onContainerMouseDown.bind(this)

    this._moveHandler = this._onMove.bind(this)
    this._endHandler = this._onEnd.bind(this)

    this._elWrapper = document.createElement('div')
    this._elContainer = document.createElement('div')
    this._elTrack = document.createElement('div')
    this._elProgress = document.createElement('div')
    this._elThumbContainer = document.createElement('div')
    this._elThumb = document.createElement('div')

    this._elWrapper.classList.add('grsw')
    this._elContainer.classList.add('grsc')
    this._elTrack.classList.add('grstr')
    this._elProgress.classList.add('grspr')
    this._elThumbContainer.classList.add('grsthc')
    this._elThumb.classList.add('grsth')

    this._elContainer.appendChild(this._elTrack)
    this._elThumbContainer.appendChild(this._elThumb)
    this._elContainer.appendChild(this._elThumbContainer)
    this._elWrapper.appendChild(this._elContainer)

    this._parentElement = null

    // Event variables

    this._touchId = undefined
    this._width = 0
    this._height = 0
    this._xMin = 0
    this._yMin = 0
    this._xMax = 0
    this._yMax = 0

    // Properties

    this._isVertical = false
    this._reverse = false
    this._min = 0
    this._max = 100
    this._step = 0.1
    this._sliderValue = 0.5
    this._progress = false
    this._start = this._min
    this._disabled = false

    this._setValueHook = null

    this._onChangeListeners = []
    this._onChangedListeners = []

    this['config'](options)

    this._attachToParent()
  }

  Object.defineProperty(RangeSlider.prototype, 'disabled', {
    get: function () {
      return this._disabled
    },
    set: function (value) {
      this._disabled = !!value
      this._processDisabled()
    }
  })

  /**
   * Parse RangeSlider options object
   *
   * @param {Object} [options]
   */
  RangeSlider.prototype['config'] = function (options) {
    if (typeof options === 'object' && options) {
      // Element
      if (typeof options['element'] === 'string') {
        this._parentElement = document.getElementById(options['element'])
      } else if (options['element'] &&
        options['element'].contains &&
        options['element'].appendChild) {
        this._parentElement = options['element']
      }
      // Properties
      if (options['orientation'] === 'horizontal' ||
          options['orientation'] === 'vertical') {
        this._isVertical = options['orientation'] === 'vertical'
      }
      if (typeof options['reverse'] === 'boolean') {
        this._reverse = options['reverse']
      }
      if (typeof options['min'] === 'number' && isFinite(options['min'])) {
        this._min = options['min']
      }
      if (typeof options['max'] === 'number' && isFinite(options['max'])) {
        this._max = options['max']
      }
      if (typeof options['step'] === 'number' && isFinite(options['step'])) {
        this._step = options['step']
      }
      if (typeof options['progress'] === 'boolean') {
        this._progress = options['progress']
      }
      if (typeof options['start'] === 'number' && isFinite(options['start'])) {
        this._start = options['start']
      }
      if (typeof options['disabled'] === 'boolean') {
        this._disabled = options['disabled']
      }
      if (typeof options['setValueHook'] === 'function') {
        this['setValueHook'](options['setValueHook'])
      }
      if (typeof options['onChange'] === 'function') {
        this['onChange'](options['onChange'])
      }
      if (typeof options['onChanged'] === 'function') {
        this['onChanged'](options['onChanged'])
      }
      if (typeof options['value'] === 'number' &&
          isFinite(options['value'])) {
        // Prepare for normalize
        this._range = this._max - this._min
        this._sliderValue = this._normalize(options['value'])
      }
    }
    this._processOptions()
  }

  RangeSlider.prototype['setValueHook'] = function (hook) {
    if (typeof hook === 'function') {
      this._setValueHook = hook
    }
  }

  RangeSlider.prototype['onChange'] = function (callback) {
    if (typeof callback === 'function') {
      this._onChangeListeners.push(callback)
    }
  }

  RangeSlider.prototype['onChanged'] = function (callback) {
    if (typeof callback === 'function') {
      this._onChangedListeners.push(callback)
    }
  }

  RangeSlider.prototype['offChange'] = function (callback) {
    var length, i
    length = this._onChangeListeners.length
    for (i = 0; i < length; i++) {
      if (this._onChangeListeners[i] === callback) {
        this._onChangeListeners.splice(i, 1)
      }
    }
  }

  RangeSlider.prototype['offChanged'] = function (callback) {
    var length, i
    length = this._onChangedListeners.length
    for (i = 0; i < length; i++) {
      if (this._onChangedListeners[i] === callback) {
        this._onChangedListeners.splice(i, 1)
      }
    }
  }

  RangeSlider.prototype['removeAllChangelisteners'] = function () {
    this._onChangeListeners = []
  }

  RangeSlider.prototype['removeAllChangedListeners'] = function () {
    this._onChangedListeners = []
  }

  RangeSlider.prototype._executeChangeListeners = function (value) {
    var length, i
    length = this._onChangeListeners.length
    for (i = 0; i < length; i++) this._onChangeListeners[i](value)
  }

  RangeSlider.prototype._executeChangedListeners = function (value) {
    var length, i
    length = this._onChangedListeners.length
    for (i = 0; i < length; i++) this._onChangedListeners[i](value)
  }

  RangeSlider.prototype._processOptions = function () {
    if (this._progress) {
      if (!this._elTrack.contains(this._elProgress)) {
        this._elTrack.appendChild(this._elProgress)
      }
    } else {
      if (this._elTrack.contains(this._elProgress)) {
        this._elTrack.removeChild(this._elProgress)
      }
    }
    if (this._isVertical) {
      this._elWrapper.classList.remove('grshor')
      this._elWrapper.classList.add('grsver')
    } else {
      this._elWrapper.classList.remove('grsver')
      this._elWrapper.classList.add('grshor')
    }
    this._processDisabled()
    this._range = this._max - this._min
    this._normalizedStep = this._step / this._range
    this._normalizedStart = this._normalize(this._start)
    this._setStart()
    this._syncUi()
  }

  RangeSlider.prototype._processDisabled = function () {
    if (this._disabled) {
      this._removeStartListeners()
      this._endAction()
      this._elWrapper.classList.add('grs-dis')
    } else {
      this._elWrapper.classList.remove('grs-dis')
      this._setStartListeners()
    }
  }

  RangeSlider.prototype._normalize = function (value) {
    return (value - this._min) / this._range
  }

  RangeSlider.prototype._denormalize = function (value) {
    return value * this._range + this._min
  }

  RangeSlider.prototype._setStart = function () {
    var percent
    percent = this._normalizedStart * 100
    if (this._reverse) percent = Math.abs(percent - 100)
    if (this._progress) {
      if (this._isVertical) {
        this._elProgress.style.top = percent + '%'
      } else {
        this._elProgress.style.left = percent + '%'
      }
    }
  }

  RangeSlider.prototype._normalizedRoundToStep = function (normalizedValue) {
    return Math.round(normalizedValue / this._normalizedStep) *
      this._normalizedStep
  }

  RangeSlider.prototype._roundToStep = function (value) {
    return Math.round(value / this._step) * this._step
  }

  RangeSlider.prototype._syncUi = function () {
    var orientation, value
    orientation = this._isVertical ? 'Y' : 'X'
    value = this._sliderValue * 100
    if (this._reverse) value = Math.abs(value - 100)
    this._elThumbContainer.style.transform = 'translate' + orientation + '(' +
      value + '%)'
    if (this._elProgress) {
      value = this._sliderValue - this._normalizedStart
      if (this._reverse) value *= -1
      this._elProgress.style.transform = 'scale' + orientation + '(' +
        value + ')'
    }
  }

  RangeSlider.prototype['setValue'] = function (value) {
    var hookResult, _value
    if (typeof value === 'number' && isFinite(value)) {
      hookResult = typeof this._setValueHook === 'function'
        ? this._setValueHook(this['getValue'](), value)
        : undefined
      _value = hookResult !== undefined ? hookResult : value
      if (typeof _value === 'number' && isFinite(_value)) {
        this._sliderValue = this._normalizedRoundToStep(this._normalize(_value))
        this._syncUi()
      }
    }
  }

  RangeSlider.prototype['getValue'] = function () {
    return this._roundToStep(this._denormalize(this._sliderValue))
  }

  /**
   * Appends the slider to the parent element
   *
   * @private
   */
  RangeSlider.prototype._attachToParent = function () {
    if (this._parentElement &&
        !this._parentElement.contains(this._elWrapper)) {
      this._parentElement.appendChild(this._elWrapper)
    }
  }

  RangeSlider.prototype._detachFromParent = function () {
    if (this._parentElement &&
        this._parentElement.contains(this._elWrapper)) {
      this._parentElement.removeChild(this._elWrapper)
    }
  }

  /**
   * @param {TouchEvent} event
   */
  RangeSlider.prototype._onThumbTouchStart = function (event) {
    this._touchStart(this._elThumb, event)
  }

  /**
   * @param {TouchEvent} event
   */
  RangeSlider.prototype._onContainerTouchStart = function (event) {
    this._touchStart(this._elContainer, event)
  }

  /**
   * @param {HTMLElement} element
   * @param {TouchEvent} event
   */
  RangeSlider.prototype._touchStart = function (element, event) {
    var touch
    if (this._touchId === undefined &&
        event.changedTouches &&
        event.changedTouches.length === 1) {
      touch = event.changedTouches[0]
      this._touchId = touch.identifier
      this._setTouchListeners(element)
      this._onStart(event, touch)
    }
  }

  /**
   * @param {MouseEvent} event
   */
  RangeSlider.prototype._onThumbMouseDown = function (event) {
    this._setGlobalMouseListeners()
    this._onStart(event)
  }

  /**
   * @param {MouseEvent} event
   */
  RangeSlider.prototype._onContainerMouseDown = function (event) {
    this._setGlobalMouseListeners()
    this._onStart(event)
  }

  /**
   * @param {(MouseEvent|TouchEvent)} event
   * @param {?Touch} [touch]
   */
  RangeSlider.prototype._onStart = function (event, touch) {
    var rect, windowScrollX, windowScrollY

    event.stopImmediatePropagation()
    if (event.cancelable) event.preventDefault()

    this._elWrapper.classList.add('grs-a')

    windowScrollX = window.scrollX
    windowScrollY = window.scrollY
    rect = this._elTrack.getBoundingClientRect()
    this._width = rect.width
    this._height = rect.height
    this._xMin = rect.left + windowScrollX
    this._yMin = rect.top + windowScrollY
    this._xMax = this._xMin + this._width
    this._yMax = this._yMin + this._height

    this._moveThumb(event, touch)
  }

  /**
   * @param {(MouseEvent|TouchEvent)} event
   */
  RangeSlider.prototype._onMove = function (event) {
    var touch, executeMove
    executeMove = false
    if (typeof this._touchId === 'number') {
      touch = getTouch(event, this._touchId)
      if (touch) executeMove = true
    } else {
      executeMove = true
    }
    if (executeMove) {
      event.stopImmediatePropagation()
      if (event.cancelable) event.preventDefault()
      this._moveThumb(event, touch)
    }
  }

  /**
   * @param {(MouseEvent|TouchEvent)} event
   */
  RangeSlider.prototype._onEnd = function (event) {
    var touch
    if (typeof this._touchId === 'number') {
      if (event.changedTouches) {
        touch = getTouch(event, this._touchId)
        if (touch) this._end()
      } else {
        console.warn('No end for touch event')
      }
    } else {
      this._end()
    }
  }

  RangeSlider.prototype._endAction = function () {
    this._touchId = undefined
    this._removeTouchListeners()
    this._removeGlobalMouseListeners()
    this._elWrapper.classList.remove('grs-a')
  }

  RangeSlider.prototype._end = function () {
    this._endAction()
    this._executeChangedListeners(this['getValue']())
  }

  /**
   * @private
   * @param {(MouseEvent|TouchEvent)} event
   * @param {?Touch} [touch]
   */
  RangeSlider.prototype._moveThumb = function (event, touch) {
    var point, value
    if (this._isVertical) {
      point = touch ? touch.pageY : event.pageY
      if (point > this._yMax) {
        this._sliderValue = this._reverse ? 0 : 1
      } else if (point < this._yMin) {
        this._sliderValue = this._reverse ? 1 : 0
      } else {
        value = (point - this._yMin) / this._height
        if (this._reverse) value = Math.abs(value - 1)
        this._sliderValue = this._normalizedRoundToStep(value)
      }
    } else {
      point = touch ? touch.pageX : event.pageX
      if (point > this._xMax) {
        this._sliderValue = this._reverse ? 0 : 1
      } else if (point < this._xMin) {
        this._sliderValue = this._reverse ? 1 : 0
      } else {
        value = (point - this._xMin) / this._width
        if (this._reverse) value = Math.abs(value - 1)
        this._sliderValue = this._normalizedRoundToStep(value)
      }
    }
    this._syncUi()
    this._executeChangeListeners(this['getValue']())
  }

  RangeSlider.prototype._setStartListeners = function () {
    this._elThumb.addEventListener(
      'touchstart',
      this._thumbTouchStartHandler,
      _activeListenerOptions
    )
    this._elContainer.addEventListener(
      'touchstart',
      this._containerTouchStartHandler,
      _activeListenerOptions
    )
    this._elThumb.addEventListener(
      'mousedown',
      this._thumbMouseDownHandler,
      _activeListenerOptions
    )
    this._elContainer.addEventListener(
      'mousedown',
      this._containerMouseDownHandler,
      _activeListenerOptions
    )
  }

  RangeSlider.prototype._removeStartListeners = function () {
    this._elThumb.removeEventListener(
      'touchstart',
      this._thumbTouchStartHandler,
      _activeListenerOptions
    )
    this._elContainer.removeEventListener(
      'touchstart',
      this._containerTouchStartHandler,
      _activeListenerOptions
    )
    this._elThumb.removeEventListener(
      'mousedown',
      this._thumbMouseDownHandler,
      _activeListenerOptions
    )
    this._elContainer.removeEventListener(
      'mousedown',
      this._containerMouseDownHandler,
      _activeListenerOptions
    )
  }

  /**
   * Sets the global touch event listeners
   *
   * @private
   * @param {HTMLElement} element
   */
  RangeSlider.prototype._setTouchListeners = function (element) {
    element.addEventListener(
      'touchend',
      this._endHandler,
      _passiveListenerOptions
    )
    element.addEventListener(
      'touchcancel',
      this._endHandler,
      _passiveListenerOptions
    )
    element.addEventListener(
      'touchmove',
      this._moveHandler,
      _activeListenerOptions
    )
  }

  /**
   * Sets the golbal mouse event listeners
   *
   * @private
   */
  RangeSlider.prototype._setGlobalMouseListeners = function () {
    window.addEventListener(
      'mouseup',
      this._endHandler,
      _passiveListenerOptions
    )
    window.addEventListener(
      'mousemove',
      this._moveHandler,
      _activeListenerOptions
    )
  }

  /**
   * Remove the global touch listeners
   *
   * @private
   */
  RangeSlider.prototype._removeTouchListeners = function () {
    this._elThumb.removeEventListener(
      'touchmove',
      this._moveHandler,
      _activeListenerOptions
    )
    this._elThumb.removeEventListener(
      'touchcancel',
      this._endHandler,
      _passiveListenerOptions
    )
    this._elThumb.removeEventListener(
      'touchend',
      this._endHandler,
      _passiveListenerOptions
    )
    this._elContainer.removeEventListener(
      'touchmove',
      this._moveHandler,
      _activeListenerOptions
    )
    this._elContainer.removeEventListener(
      'touchcancel',
      this._endHandler,
      _passiveListenerOptions
    )
    this._elContainer.removeEventListener(
      'touchend',
      this._endHandler,
      _passiveListenerOptions
    )
  }

  /**
   * Remove the global mouse listeners
   *
   * @private
   */
  RangeSlider.prototype._removeGlobalMouseListeners = function () {
    window.removeEventListener(
      'mousemove',
      this._moveHandler,
      _activeListenerOptions
    )
    window.removeEventListener(
      'mouseup',
      this._endHandler,
      _passiveListenerOptions
    )
  }

  RangeSlider.prototype['destroy'] = function () {
    this._removeStartListeners()
    this._endAction()
    this['removeAllChangelisteners']()
    this['removeAllChangedListeners']()
    this._detachFromParent()
    this._parentElement = null
    this._setValueHook = null
  }

  return RangeSlider
}))
